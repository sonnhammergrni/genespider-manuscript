;; Minimal setup for compiling genespider org-mode document
;; run:
;; emacs -q -l path/to-this/org-compile-setup.el
;;
;; let emacs do its thing. If error close emacs and open it with the same command.
;; For a permanent solution place the following code in emacs initialisation file.
;; Don't modify this code in that case. Move it and modify it.
;;
;; when ready to compile press the key combination C-c C-e l o.
;; That is `ctrl c` at the same time and `ctrl e` at the same time, then one can
;; follow the instructions if one wish or press l for latex and o for compile and show output.


(require 'cl)
(setq emacs-init-dir
      (file-name-directory (or load-file-name (buffer-file-name))))

(setq el-get-dir (expand-file-name "el-get" emacs-init-dir))
(add-to-list 'load-path (concat emacs-init-dir "el-get/el-get"))

(unless (require 'el-get nil 'noerror)
  (with-current-buffer
      (url-retrieve-synchronously
       "https://raw.github.com/dimitri/el-get/master/el-get-install.el")
    (let (el-get-master-branch)
      (goto-char (point-max))
      (eval-print-last-sexp))))

(el-get 'sync (list "org-mode"))
(require 'ox-latex)

(el-get-bundle helm-bibtex
       :description "Manage bibliographies in Emacs"
       :wepage "https://github.com/tmalsburg/helm-bibtex"
       :type github
       :pkgname "tmalsburg/helm-bibtex"
       :depends (helm))
(el-get-bundle org-ref
       :description "Org-ref - The best reference handling for org-mode"
       :website "https://github.com/Xparx/org-ref"
       :type github
       :pkgname "Xparx/org-ref"
       :depends (reftex dash s f ebib helm-bibtex hydra key-chord)
       :features "org-ref")
(el-get 'sync (list "org-ref"))

(add-to-list 'org-latex-classes '("bare-article" "\\documentclass[11pt]{article}
[NO-DEFAULT-PACKAGES]
[PACKAGES]
[EXTRA]"
  ("\\section{%s}" . "\\section*{%s}")
  ("\\subsection{%s}" . "\\subsection*{%s}")
  ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
  ("\\paragraph{%s}" . "\\paragraph*{%s}")
  ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))

(defun org-remove-headlines (backend)
  "Remove headlines with :notitle: tag."
  (org-map-entries (lambda () (let ((beg (point)))
                                (outline-next-visible-heading 1)
                                (backward-char)
                                (delete-region beg (point))))
                   "noexport" tree)
  (org-map-entries (lambda () (delete-region (point-at-bol) (point-at-eol)))
                   "notitle"))

(add-hook 'org-export-before-processing-hook #'org-remove-headlines)


(setq texcmd "latexmk -pdf -bibtex -f -shell-escape -quiet %f")

(setq org-export-with-sub-superscripts nil)
(setq org-export-copy-to-kill-ring nil)
(setq org-latex-prefer-user-labels t)
(setq org-babel-load-languages '((emacs-lisp . t)
                                 (sh . t)
                                 (matlab . t)))

(el-get 'sync '(matlab-mode))

(when (el-get-package-exists-p "matlab-mode")
  (add-hook 'matlab-mode-hook '(lambda () (auto-fill-mode -1))))
(when (el-get-package-exists-p "matlab-mode")
  (add-to-list 'auto-mode-alist '("\\.m$" . matlab-mode))
  (setq matlab-indent-function nil)
  (setq matlab-shell-command "matlab"))


;; if the file opens in emacs configure this code (set "evince" to something appropriate) and uncomment
;; (eval-after-load "org" '(progn (setcdr (assoc "\\.pdf\\'" org-file-apps) "evince %s")))
