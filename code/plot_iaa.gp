#! /usr/bin/gnuplot
reset

outdir = '../img/'

data1 = '../data/random_iaa.dat'
data2 = '../data/smallworld_iaa.dat'
data3 = '../data/scalefree_iaa.dat'
data4 = '../data/smallworld_scalefree_iaa.dat'

# if (!exists("data")) data='../data/random_properties.dat'
# outfile = system('basename '.data.' .dat | tr -d "."')
outfile = 'iaa_networks'


system('echo '.data1)
system('echo '.outfile)

set encoding utf8
# set terminal wxt size 700,500 enhanced font 'Verdana,10'
set terminal svg size 900,600 fname 'Verdana' fsize "12" linewidth 2 enhanced; set output outdir.outfile.".svg"

# load "moreland.pal"

# Axes
set style line 101 lc rgb '#808080' lt 1
set border 3 back ls 101
set tics nomirror out scale 0.75

set ytics nomirror
# Grid
set style line 102 lc rgb '#808080' lt 0 lw 0.4
set grid ytics xtics mxtics back ls 102

# set style line 103 lc rgb '#808080' lt 0 lw 0.2
# set grid mxtics ls 103

# Dots
set style line 1 lc rgb '#60ad00' pt 7 ps 0.7 lt 1 lw 1 # --- green

unset key

unset y2tics
XRANGE = "set xrange [0:20]"
XTICS = "set xtics format '%.0f'"
YTICS_LOG = "set logscale y; set format y '10^{%L}'; set yrange [1:10000]"
NOXTICS = "set xtics format ''"
NOYTICS = "set ytics format ''"

XLABEL = "set xlabel 'Data set'"
NOXLABEL = "unset xlabel"
YLABEL = "set ylabel 'IAA'"
NOYLABEL = "unset ylabel"

SIZE11 = "set size 0.32,0.25"
SIZE12 = "set size 0.28,0.25"
SIZE13 = "set size 0.28,0.25"
SIZE21 = "set size 0.32,0.32"
SIZE22 = "set size 0.28,0.25"
SIZE23 = "set size 0.28,0.25"
SIZE32 = "set size 0.32,0.25"
SIZE33 = "set size 0.28,0.25"
SIZE42 = "set size 0.32,0.32"
SIZE43 = "set size 0.28,0.32"

ORIGIN11 = "set origin (0.1),(3.0/4-0.02)"
ORIGIN12 = "set origin (1.0/3+0.09),(3.0/4-0.02)"
ORIGIN13 = "set origin (2.0/3+0.02),(3.0/4-0.02)"
ORIGIN21 = "set origin (0.1),(1.0/2-0.055)"
ORIGIN22 = "set origin (1.0/3+0.09),(1.0/2+0.01)"
ORIGIN23 = "set origin (2.0/3+0.02),(1.0/2+0.01)"
ORIGIN32 = "set origin (1.0/3+0.05),(1.0/4+0.04)"
ORIGIN33 = "set origin (2.0/3+0.02),(1.0/4+0.04)"
ORIGIN42 = "set origin (1.0/3+0.05),0.00"
ORIGIN43 = "set origin (2.0/3+0.02),0.00"

set label "Random"  at screen 0.08, screen 0.9 center rotate by 90
set label "Small-world"  at screen 0.08, screen 0.66 center rotate by 90
set label "Scale-free"  at screen 0.08, screen 0.4 center rotate by 90
set label sprintf("Small-world\nScale-free")  at screen 0.08, screen 0.15 center rotate by 90


set label "N=10"   at screen 0.26,  screen 0.98
set label "N=50"   at screen 0.535, screen 0.98
set label "N=100"  at screen 0.8,  screen 0.98

set multiplot layout 4,3

#################### linear plots ###################
@XTICS;
@XRANGE;

#################### plots 11 ###################
@SIZE11; @NOXTICS; @YTICS_LOG; @ORIGIN11; @NOXLABEL; @YLABEL
plot data1 index 0 using 1:2 with points axis x1y1 ls 1

#################### plots 12 ###################
@SIZE12; @NOXTICS; @NOYTICS; @ORIGIN12; @NOXLABEL; @NOYLABEL
plot data1 index 1 using 1:2 with points axis x1y1 ls 1

#################### plots 13 ###################
@SIZE13; @NOXTICS; @NOYTICS; @ORIGIN13; @NOXLABEL; @NOYLABEL
plot data1 index 2 using 1:2 with points axis x1y1 ls 1

unset title
#################### plots 21 ###################
@SIZE21; @XTICS; @YTICS_LOG; @ORIGIN21; @XLABEL; @YLABEL
plot data2 index 0 using 1:2 with points axis x1y1 ls 1

#################### plots 22 ###################
@SIZE22; @NOXTICS; @NOYTICS; @ORIGIN22; @NOXLABEL; @NOYLABEL
plot data2 index 1 using 1:2 with points axis x1y1 ls 1

#################### plots 23 ###################
@SIZE23; @NOXTICS; @NOYTICS; @ORIGIN23; @NOXLABEL; @NOYLABEL
plot data2 index 2 using 1:2 with points axis x1y1 ls 1

#################### plots 32 ###################
@SIZE32; @YTICS_LOG; @NOXTICS; @ORIGIN32; @NOXLABEL; @YLABEL
plot data3 index 1 using 1:2 with points axis x1y1 ls 1

#################### plots 33 ###################
@SIZE33; @NOYTICS; @NOXTICS; @ORIGIN33; @NOXLABEL; @NOYLABEL
plot data3 index 2 using 1:2 with points axis x1y1 ls 1

@XTICS
#################### plots 42 ###################
@SIZE42; @YTICS_LOG; @ORIGIN42; @XLABEL; @YLABEL
plot data4 index 0 using 1:2 with points axis x1y1 ls 1

#################### plots 43 ###################
@SIZE43; @NOYTICS; @ORIGIN43; @XLABEL; @NOYLABEL
plot data4 index 1 using 1:2 with points axis x1y1 ls 1


unset multiplot

unset output

# system('wait %1')
system('inkscape -f '.outdir.outfile.'.svg -A '.outdir.outfile.'.pdf')
# system('pdfcrop '.outdir.outfile.'.pdf')
# system('mv '.outdir.outfile.'.pdf')
