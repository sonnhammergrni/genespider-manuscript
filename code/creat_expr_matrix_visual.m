% Script for creating a visually nice looking expression matrix plot
rerandom = false;

C = exp_colormap();


colormap(C);
size_block_one = [10,3];
size_block_two = [10,6];

if rerandom
    a = rand(size_block_one)*2+3;
    b = rand(size_block_one)*0.01;
    c = rand(size_block_two)*4;
    d = rand(size_block_two)-2;

    f = full(sprandn(size_block_two(1),size_block_two(2),0.25))*2;
    d(find(f)) = f(find(f))-1;

    e = full(sprandn(size_block_one(1),size_block_one(2),0.25))*2;
    b(find(e)) = e(find(e))+1;


    M = [a,d;b,c];
end

imagesc(M);
set(gca,'YTick',[])
set(gca,'XTick',[])
set(gca,'YTickLabel',{})
set(gca,'XTickLabel',{})
% set(gcf,'Position', [3392,391,560,837])
% set(gcf,'PaperPosition', [0.25, 2.5, 8, 6])

saveas(gcf,'../img/expression_array','epsc')
