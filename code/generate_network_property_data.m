% Script for generating data of network properties
% set(0,'DefaultFigureVisible','off')

% % to show figures do
% set(0,'DefaultFigureVisible','on');
% figure(h)
% % where h is a figure handle.
% % Also works with figure handle arrays h = [h(1), h(2)]

save_data = false;
save_nets = true;

datadir='/afs/pdc.kth.se/misc/pdc/volumes/sbc/prj.sbc.sparx.2/Benchmark/data/networks';

creator = 'Tjarnberg';
analyse.Model.type('directed')

types = {'random','smallworld','scalefree','smallworld_scalefree'};

for l=1:length(types)
    %% random networks
    sizes = [10,50,100];
    netdir = types{l};
    k = 0;

    for j=1:length(sizes)
        k=k+1;
        sizeofnet = sizes(j);

        % Grab the correct networks
        filedir = fullfile(datadir,netdir,['N',num2str(sizeofnet)]);
        if ~isdir(filedir)
            continue
        end
        datasets = datastruct.Network.load(filedir);
        use_data = strncmp(creator,datasets,length(creator));
        datasets = datasets(use_data);

        n = 1;
        for i=1:length(datasets)
            [junk,junk,ext] = fileparts(datasets{i});

            if strcmp('.mat',ext)
                tmp{n} = datasets{i};
                n = n+1;
            end
        end
        datasets = tmp; clear tmp n use_data i
        %

        c_coeff = []; degree_dist = [];
        mean_c_coeff = []; mdegree = [];
        iaa = [];
        smw = [];
        o = 1;
        for i=1:length(datasets)
            net = datastruct.Network.load(fullfile(filedir,datasets{i}));
            tmp = analyse.Model(net);
            if tmp.interampatteness > 0.4*sizes(j) % remove those with ultra small iaa

                c_coeff(:,o) = analyse.Model.clustering_coefficient(net);
                degree_dist(:,o) = analyse.Model.degree_distribution(net);

                iaa(o) = tmp.interampatteness;
                smw(o) = tmp.proximity_ratio;
                mean_degree(o) = tmp.DD;
                mean_c_coeff(o) = tmp.CC;
                net_comp(o) = tmp.networkComponents;
                median_path_l(o) = tmp.medianPathLength;
                mean_path_l(o) = tmp.meanPathLength;
                o = o + 1;

                if save_nets
                    new_net_dir = fullfile('../data/networks',netdir,num2str(sizeofnet));
                    if ~isdir(new_net_dir)
                        [ble,junk] = mkdir(new_net_dir);
                    end
                    net.description = ['Stable ',netdir,' network, IAA=',num2str(tmp.interampatteness)];
                    save(net,new_net_dir,'.json')
                end
            end
        end
        clear tmp

        if ~((l == 3) & (j == 1))
            IAA{l,j} = iaa;
            SMW{l,j} = smw;
            smwness(l,j) = mean(smw);
            m_CC{l,j} = mean_c_coeff;
            m_DD{l,j} = mean_degree;
            m_NC{l,j} = net_comp;
        end
        d_out_max = max(degree_dist(:));

        [freq,bins] = hist(degree_dist(:),[0:d_out_max+1]);

        freq = freq/sum(freq);

        if save_data
            plot_data = '../data/';
            f_name = [netdir,'_properties.dat'];
            gsUtilities.export2gnuplot(fullfile(plot_data,f_name),{['out degree distribution, N=',num2str(sizeofnet)]},[],{'bins','freq'},[bins',freq']);
            f_name = [netdir,'_iaa.dat'];
            gsUtilities.export2gnuplot(fullfile(plot_data,f_name),{['interampatteness degree, N=',num2str(sizeofnet)]},[],{'net','iaa'},[[1:length(iaa)]',sort(iaa)']);
        end
        plotdata{l,j} = [bins',freq'];
    end
end
return
[L,J] = size(plotdata);

types_names = types;
types_names{4} = {'smallworld';'scalefree'};

%% Plot degree distribution
h = figure();

% XTicks{1,1}

k = 1;
for l=1:L
    for j=1:J
        if ~and(l==4,j==1) && ~and(l==3,j==1)
            sh(k) = subplot(L,J,k);
            d = plotdata{l,j};

            if strcmp('scalefree',types{l}) || strcmp('smallworld_scalefree',types{l})
                loglog(d(:,1),d(:,2),'.')
                % xl = xlim();
                % yl = ylim();
                % xtick = [xl(1):1:xl(2)];
                % ytick = [yl(1):2:yl(2)];
                xtick = get(gca,'XTick');
                ytick = get(gca,'YTick');
            else
                plot(d(:,1),d(:,2),'.')
                % xl = xlim();
                % yl = ylim();
                xtick = [0:2:10];
                ytick = [0:0.1:0.5];
                xlim([0,10]);
            end
            set(gca,'XTick',xtick);
            set(gca,'YTick',ytick);

            if (j == 1 && l < 3) || (j == 2 && l >= 3)
                % ylabel(types_names{l})
            end
            if l == 4
                xlabel(['N=',num2str(sizes(j))])
            end

            grid on
            if l == 1
                set(gca,'XTickLabel',{''})
            end
        end
        k = k + 1;
    end

    if l < 3
        t = text(-31,0.00,types_names{l});
        set(t,'Rotation',90);
    else
        t = text(0.0000005,0.0002,types_names{l});
        set(t,'Rotation',90);
    end
end
% suptitle('out degree distribution')
% saveas(h,'../img/out_degree_distribution.pdf','pdf')

%% Plot sorted interampatteness degree
h = figure();
k = 1;
for l=1:L
    for j=1:J
        if ~isempty(IAA{l,j})
            subplot(L,J,k)
            d = IAA{l,j};
            semilogy([1:length(d)],sort(d),'g.')
            grid on
            xlim([0,length(d)+1])

            ax = gca;

            ax.YLim = [1,10000];
            ax.XLim = [0,20];

            if j == 1
                ylabel(types_names{l})
            end
            if l == 1
                title(['N=',num2str(sizes(j))])
            end
        end

        k = k + 1;
    end
end
% suptitle('interampatteness degree')
% saveas(h,'../img/interampatteness_degree.pdf','pdf')
