#! /usr/bin/gnuplot
reset

outdir = '../img/'

data1 = '../data/random_performance.dat'
data2 = '../data/smallworld_performance.dat'
data3 = '../data/scalefree_performance.dat'
data4 = '../data/smallworld_scalefree_performance.dat'

outfile = 'data_performance'


system('echo '.data1)
system('echo '.outfile)

set encoding utf8
set terminal svg size 900,600 fname 'Verdana' fsize "12" linewidth 2 enhanced; set output outdir.outfile.".svg"

# Axes
set style line 101 lc rgb '#808080' lt 1
set border 3 back ls 101
set tics nomirror out scale 0.75

set ytics nomirror
# Grid
set style line 102 lc rgb '#808080' lt 0 lw 0.4
# set grid ytics xtics mxtics back ls 102
set grid ytics back ls 102

# set style line 103 lc rgb '#808080' lt 0 lw 0.2
# set grid mxtics ls 103

set style line 1 lc rgb '#e41a1c' pt 1 ps 0.8 lt 1 lw 1 # ---
set style line 2 lc rgb '#377eb8' pt 2 ps 0.8 lt 1 lw 1 # ---
set style line 3 lc rgb '#4daf4a' pt 8 ps 0.8 lt 1 lw 1 # ---
set style line 4 lc rgb '#984ea3' pt 6 ps 0.8 lt 1 lw 1 # ---

unset y2tics
XRANGE = "set xrange [-1:6]"
XTICS = "set xtic rotate by -90 scale 0.5  offset character -1, 0, 0; set xtics font ',12'; set xtics ('0.01' 0, '1' 1,'100' 2, '0.01' 3, '1' 4, '100' 5)"
YTICS   = "set yrange [0:1]; set ytics format '%.1f'"
NOXTICS = "set xtics format '' scale 0.5; set xtics font ',0'; set xtics ('0.01' 0, '1' 1,'100' 2, '0.01' 3, '1' 4, '100' 5)"
NOYTICS = "set ytics format '' scale 0.5"
XLABEL = "set xlabel 'SNR'"
NOXLABEL = "unset xlabel"
YLABEL = "set ylabel 'MCC'"
NOYLABEL = "unset ylabel"


SIZE1 = "set size 0.3,0.26"
SIZE2 = "set size 0.32,0.26"
SIZE3 = "set size 0.3,0.25"

SIZE11 = "set size 0.32,0.26"
SIZE12 = "set size 0.28,0.26"
SIZE21 = "set size 0.32,0.32"
SIZE22 = "set size 0.275,0.28"
SIZE23 = "set size 0.3,0.28"
SIZE31 = "set size 0.32,0.28"
SIZE32 = "set size 0.3,0.28"
SIZE42 = "set size 0.3,0.32"
SIZE41 = "set size 0.32,0.32"


ORIGIN11 = "set origin (0.1),(3.0/4-0.05)"
ORIGIN12 = "set origin (1.0/3+0.08),(3.0/4-0.05)"
ORIGIN13 = "set origin (2.0/3),(3.0/4-0.05)"
ORIGIN21 = "set origin (0.1),(2.0/4-0.07)"
ORIGIN22 = "set origin (1.0/3+0.08),(1.0/2-0.03)"
ORIGIN23 = "set origin (2.0/3),(2.0/4-0.03)"
ORIGIN32 = "set origin (1.0/3+0.03),(1.0/4)"
ORIGIN33 = "set origin (2.0/3),(1.0/4)"
ORIGIN42 = "set origin (1.0/3+0.03),0"
ORIGIN43 = "set origin (2.0/3),0"

set label "Random"  at screen 0.08, screen 0.85 center rotate by 90
set label "Small-world"  at screen 0.08, screen 0.66 center rotate by 90
set label "Scale-free"  at screen 0.08, screen 0.4 center rotate by 90
set label sprintf("Small-world\nScale-free") at screen 0.08, screen 0.15 center rotate by 90

set label "N=10"   at screen 0.26,  screen 0.98
set label "N=50"   at screen 0.535, screen 0.98
set label "N=100"  at screen 0.8,  screen 0.98

set label 'high IAA' at screen 0.24, screen 0.95 center back textcolor ls 102
set label 'high IAA' at screen 0.5, screen 0.95 center back textcolor ls 102
set label 'high IAA' at screen 0.76, screen 0.95 center back textcolor ls 102
set label 'low IAA' at screen 0.33, screen 0.95 center back textcolor ls 102
set label 'low IAA' at screen 0.60, screen 0.95 center back textcolor ls 102
set label 'low IAA' at screen 0.87, screen 0.95 center back textcolor ls 102



unset border
set auto x
set yrange [0:1]
set style data histogram
set style histogram cluster gap 1
set style fill solid noborder 1.0
set boxwidth 0.9

set multiplot layout 4,3
@YTICS;
@XRANGE;
# unset key

unset key
#################### plots 11 ###################
@SIZE11; @ORIGIN11; @NOXTICS; @YLABEL
plot data1 index 0 using 3:xtic(2) ls 2 title col, \
     "" index 0 using 4 ls 3 title col, \
     "" index 0 using 5 ls 4 title col, \

#################### plots 12 ###################
@SIZE12; @ORIGIN12; @NOXTICS; @NOYTICS; @NOYLABEL
plot data1 index 1 using 3:xtic(2) ls 2 title col, \
     "" index 1 using 4 ls 3 title col, \
     "" index 1 using 5 ls 4 title col, \

#################### plots 13 ###################
@SIZE1; @ORIGIN13; @NOXTICS; @NOYTICS; @NOYLABEL
plot data1 index 2 using 3:xtic(2) ls 2 title col, \
     "" index 2 using 4 ls 3 title col, \
     "" index 2 using 5 ls 4 title col, \

# set key out vert bot center
set key at screen 0.2, screen 0.2 center
#################### plots 21 ###################
@SIZE21; @XTICS; @YTICS; @ORIGIN21; @XLABEL; @YLABEL
plot data2 index 0 using 3:xtic(2) ls 2 title col, \
     "" index 0 using 4 ls 3 title col, \
     "" index 0 using 5 ls 4 title col, \

unset key
#################### plots 22 ###################
@SIZE22; @NOXTICS; @NOYTICS; @ORIGIN22; @NOXLABEL; @NOYLABEL
plot data2 index 1 using 3:xtic(2) ls 2 title col, \
     "" index 1 using 4 ls 3 title col, \
     "" index 1 using 5 ls 4 title col, \

#################### plots 23 ###################
@SIZE23; @NOXTICS; @NOYTICS; @ORIGIN23; @NOXLABEL; @NOYLABEL
plot data2 index 2 using 3:xtic(2) ls 2 title col, \
     "" index 2 using 4 ls 3 title col, \
     "" index 2 using 5 ls 4 title col, \

#################### plots 32 ###################
@SIZE31; @YTICS; @NOXTICS; @ORIGIN32; @NOXLABEL; @YLABEL
plot data3 index 0 using 3:xtic(2) ls 2 title col, \
     "" index 0 using 4 ls 3 title col, \
     "" index 0 using 5 ls 4 title col, \

#################### plots 33 ###################
@SIZE32; @NOYTICS; @NOXTICS; @ORIGIN33; @NOXLABEL; @NOYLABEL
plot data3 index 1 using 3:xtic(2) ls 2 title col, \
     "" index 1 using 4 ls 3 title col, \
     "" index 1 using 5 ls 4 title col, \


#################### plots 42 ###################
@SIZE41; @ORIGIN42; @YTICS; @XTICS; @XLABEL; @YLABEL
plot data4 index 0 using 3:xtic(2) ls 2 title col, \
     "" index 0 using 4 ls 3 title col, \
     "" index 0 using 5 ls 4 title col, \


#################### plots 43 ###################
@SIZE42; @NOYTICS; @ORIGIN43; @XTICS; @XLABEL; @NOYLABEL
plot data4 index 1 using 3:xtic(2) ls 2 title col, \
     "" index 1 using 4 ls 3 title col, \
     "" index 1 using 5 ls 4 title col, \

unset multiplot

unset output

# system('wait %1')
system('inkscape -f '.outdir.outfile.'.svg -A '.outdir.outfile.'.pdf')
# system('pdfcrop '.outdir.outfile.'.pdf')
# system('mv '.outdir.outfile.'.pdf')
