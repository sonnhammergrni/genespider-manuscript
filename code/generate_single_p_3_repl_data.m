% Script for generating data of network properties
% set(0,'DefaultFigureVisible','off')

% % to show figures do
% set(0,'DefaultFigureVisible','on');
% figure(h)
% % where h is a figure handle.
% % Also works with figure handle arrays h = [h(1), h(2)]
seed = 256496989;
rng(seed);

datadir='/afs/pdc.kth.se/misc/pdc/volumes/sbc/prj.sbc.sparx.2/Benchmark/data/networks';
outdir = '../data/datasets';

creator = 'Tjarnberg';
analyse.Model.type('directed')

save_performance = false;
savedata = false;
save_datasets = true;
runmethods = false;

usemethods = {'RNI','lsco','Glmnet'};
types = {'random','smallworld','scalefree','smallworld_scalefree'};
sizes = [10,50,100];
SNRs = [0.01,1,100];
MCClk = [];
MCChk = [];
E{1} = randn(sizes(1),sizes(1)*3);
E{2} = randn(sizes(2),sizes(2)*3);
E{3} = randn(sizes(3),sizes(3)*3);


for l=1:length(types)
    %% random networks
    netdir = types{l};
    k = 0;

    for j=1:length(sizes)
        if (j == 1 && strcmp(types(l),'scalefree')) || (j == 1 && strcmp(types(l),'smallworld_scalefree'))
            continue
        end
        k=k+1;
        sizeofnet = sizes(j);

        % Grab the correct networks
        filedir = fullfile(datadir,netdir,['N',num2str(sizeofnet)]);
        if ~isdir(filedir)
            continue
        end
        datasets = datastruct.Network.load(filedir);
        use_data = strncmp(creator,datasets,length(creator));
        datasets = datasets(use_data);

        n = 1;
        for i=1:length(datasets)
            [junk,junk,ext] = fileparts(datasets{i});

            if strcmp('.mat',ext)
                tmp{n} = datasets{i};
                n = n+1;
            end
        end
        datasets = tmp; clear tmp n use_data i
        %
        Highk_perforamnce = [];
        Lowk_perforamnce = [];
        snrvals = [];
        kappa = [];
        kappaval = [];
        ykappaval = [];
        for k=1:length(SNRs)
            KY = [];
            SNR_Gauss = [];
            SNR_gauss = [];
            SNR_true = [];
            e_variance = [];
            o = 1;
            lowk = true;
            highk = true;
            for i=1:length(datasets)
                net = datastruct.Network.load(fullfile(filedir,datasets{i}));
                if cond(net.A) > 0.4*sizes(j) % remove those with ultra small iaa
                    P = eye(sizes(j));
                    D(1).P = [P,P,P];

                    Y = net.G*D.P;
                    s = svd(Y);
                    stdE = s(sizes(j))/(SNRs(k)*sqrt(chi2inv(1-analyse.Data.alpha,prod(size(D.P)))));

                    D(1).network = net.network;
                    D(1).E = stdE*E{j};
                    D(1).F = zeros(size(D.P));
                    D(1).Y = Y+D.E;
                    D(1).lambda = [stdE^2,0];
                    D(1).cvY = D.lambda(1)*eye(sizes(j));
                    D(1).cvP = zeros(sizes(j));
                    D(1).sdY = stdE*ones(size(D.P));
                    D(1).sdP = zeros(size(D.P));

                    data = datastruct.Dataset(net,D);
                    setname(data,struct('creator','Tjarnberg'));

                    if save_datasets
                        if ~exist(fullfile(outdir,['N',num2str(sizes(j))]),'dir')
                            mkdir(fullfile(outdir,['N',num2str(sizes(j))]))
                        end
                        save(data,fullfile(outdir,['N',num2str(sizes(j))]),'.json')
                    end

                    iaa(o) = cond(net.A);
                    KY(o) = cond(data.Y);
                    SNR_Gauss(o) = analyse.Data.calc_SNR_Phi_gauss(data);
                    SNR_gauss(o) = min(analyse.Data.calc_SNR_phi_gauss(data));
                    SNR_true(o) = analyse.Data.calc_SNR_Phi_true(data);
                    e_variance(o) = data.lambda(1);

                    if runmethods && lowk && (iaa(o) < data.N)
                        disp('lowk calculating networks for')
                        disp('data set')
                        disp(i)
                        disp('SNR')
                        disp(SNRs(k))
                        disp('size')
                        disp(sizes(j))
                        disp('type')
                        disp(types{l})
                        disp('iaa')
                        disp(cond(net.A))
                        disp(iaa(o))
                        disp('KY')
                        disp(cond(data.Y))

                        estA = Methods.RNI(data,analyse.Data.alpha);
                        M = analyse.CompareModels(net,estA);
                        MCClk(1) = M.MCC;
                        estA = Methods.lsco(data,logspace(-6,0,100));
                        M = analyse.CompareModels(net,estA);
                        MCClk(2) = max(M.MCC);
                        estA = Methods.Glmnet(data,logspace(-6,0,100));
                        M = analyse.CompareModels(net,estA);
                        MCClk(3) = max(M.MCC);
                        Lowk_perforamnce = [Lowk_perforamnce; MCClk(1,:)];

                        snrvals = [snrvals,SNRs(k)];
                        kappa = [kappa,{'"low"'}];
                        kappaval = [kappaval,cond(net)];
                        ykappaval = [ykappaval,cond(data.Y)];

                        lowk = false;
                    end

                    if runmethods && highk && (iaa(o) > data.N*5)
                        disp('highk calculating networks for')
                        disp('data set')
                        disp(i)
                        disp('SNR')
                        disp(SNRs(k))
                        disp('size')
                        disp(sizes(j))
                        disp('type')
                        disp(types{l})
                        disp('iaa')
                        disp(cond(net.A))
                        disp(iaa(o))
                        disp('KY')
                        disp(cond(data.Y))

                        estA = Methods.RNI(data,analyse.Data.alpha);
                        M = analyse.CompareModels(net,estA);
                        MCChk(1) = max(M.MCC);
                        estA = Methods.lsco(data,logspace(-6,0,100));
                        M = analyse.CompareModels(net,estA);
                        MCChk(2) = max(M.MCC);
                        estA = Methods.Glmnet(data,logspace(-6,0,100));
                        M = analyse.CompareModels(net,estA);
                        MCChk(3) = max(M.MCC);
                        Highk_perforamnce = [Highk_perforamnce; MCChk(1,:)];

                        snrvals = [snrvals,SNRs(k)];
                        kappa = [kappa,{'"high"'}];
                        kappaval = [kappaval,cond(net)];
                        ykappaval = [ykappaval,cond(data.Y)];

                        highk = false;
                    end

                    o = o + 1;
                end
            end
            if savedata
                plot_data = '../data/';
                f_name = [netdir,'_data_props.dat'];
                header = {['interampatteness degree, N=',num2str(sizeofnet),' M=',num2str(data.M),' SNR=',num2str(SNRs(k))]};
                variables_of_interest = {'dataset','Cond Y','SNR Y gauss','SNR y gauss','SNR E true','variance','IAA'};
                [junk,ind] = sort(iaa);
                values = [];
                values = [[1:length(iaa)]',KY(ind)',SNR_Gauss(ind)',SNR_gauss(ind)',SNR_true(ind)',e_variance(ind)',iaa(ind)'];
                tools.export2gnuplot(fullfile(plot_data,f_name),header,[],variables_of_interest,values);
            end
        end

        if save_performance & runmethods
            values = {};
            plot_data = '../data/';
            f_name = [netdir,'_performance.dat'];
            header = {['Performance of..., N=',num2str(sizeofnet),' M=',num2str(data.M)]};
            method_names = regexprep(usemethods,'(\w+)','"$1"');
            performance_variables = [{'K','SNR'},method_names,{'IAA','K(Y)'}];

            high = find(strcmp('"high"',kappa));
            low = find(strcmp('"low"',kappa));

            tmp = {};
            for k=1:length(high)
                tmp{k,1} = kappa{high(k)};
                tmp{k,2} = [snrvals(high(k)),Highk_perforamnce(k,:),kappaval(high(k))',ykappaval(high(k))'];
            end
            values = [values;tmp];

            tmp = {};
            for k=1:length(low)
                tmp{k,1} = kappa{low(k)};
                tmp{k,2} = [snrvals(low(k)),Lowk_perforamnce(k,:),kappaval(low(k))',ykappaval(low(k))'];
            end
            values = [values;tmp];

            tools.export2gnuplot(fullfile(plot_data,f_name),header,[],performance_variables,values);
        end
    end
end
